Parameters for invoking CGI calls remotely:

a) Morph Analyser:
http://52.25.246.194/cgi-bin/scl/morph/morph.cgi?encoding=Unicode&morfword=रामेण

b)Noun form Generator:
http://52.25.246.194/cgi-bin/scl/skt_gen/noun/noun_gen.cgi?encoding=Unicode&rt=राम&gen=पुं&level=1

Note: gen = पुं / नपुं / स्त्री

c) Verb form Generator
http://52.25.246.194/cgi-bin/scl/skt_gen/verb/verb_gen.cgi?encoding=WX&prayoga=karwari&vb=gam1_gamLz_BvAxiH_gawO

Note: prayoga = karwari / karmaNi

For the list of possible values of vb, pl open the html file
http://sanskrit.uohyd.ac.in/skt_gen/verb/verb.html

d) Krt form generator:
http://52.25.246.194/cgi-bin/scl/skt_gen/kqw/kqw_gen.cgi?encoding=WX&vb=gam1_gamLz_BvAxiH_gawO

For the list of possible values of vb, pl open the html file
http://52.25.246.194/skt_gen/kqw/kqw.html

e) Taddhita form generator:
http://52.25.246.194/cgi-bin/scl/skt_gen/waxXiwa/waxXiwa_gen.cgi?encoding=Unicode&rt=राम

f) Sandhi joiner:
http://52.25.246.194/cgi-bin/scl/sandhi/sandhi.cgi?encoding=Unicode&text=राम&textt1=आलय

g) Sandhi splitter:
http://52.25.246.194/cgi-bin/scl/sandhi_splitter/sandhi_splitter.cgi?encoding=Unicode&word=रामालयः&sandhi_type=s

Note: sandhi_type = s, if it is a padaccheda
      sandhi_type = S, if it is a samaasaccheda

